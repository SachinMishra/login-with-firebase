package com.ms.sam.loginfirebase;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity {

    EditText et_email,et_password,et_name,et_address,et_contact;

   private FirebaseAuth mAuth;
    Button register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        et_email=findViewById(R.id.email);
        et_password=findViewById(R.id.password);
        register=findViewById(R.id.register);

        et_name=findViewById(R.id.name);
        et_address=findViewById(R.id.address);
        et_contact=findViewById(R.id.contact);



        mAuth=FirebaseAuth.getInstance();

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = et_email.getText().toString();
                String password = et_password.getText().toString();

                String name = et_name.getText().toString();
                String address = et_address.getText().toString();
                String contact = et_contact.getText().toString();

                if (name.equals(""))
                {
                    et_name.setError("please enter name");
                }
                else
                    if (address.equals(""))
                    {
                    et_address.setError("please enter address");
                }
                else
                    if (contact.equals(""))
                    {
                    et_contact.setError("please enter contact");
                }
                else
                    if (email.equals(""))
                    {
                    et_email.setError("Please enter email");
                }
                else
                    if (password.equals(""))
                    {
                    et_password.setError("please enter paasword");
                }
                else
                    {
                        final ProgressDialog pd=new ProgressDialog(RegisterActivity.this);
                        pd.setTitle("Please wait");

                        pd.show();


                    mAuth.createUserWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            if (authResult.getUser() != null) {

                                Toast.makeText(RegisterActivity.this, "Registered Successful", Toast.LENGTH_SHORT).show();

                                pd.dismiss();
                                et_email.setText("");
                                et_password.setText("");
                                et_address.setText("");
                                et_contact.setText("");
                                et_name.setText("");

                            }

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(RegisterActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

                }
            }
        });


    }
}
