package com.ms.sam.loginfirebase;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {


    SharedPreferences sp;
    SharedPreferences.Editor editor;



    EditText et_Email,et_password;
    Button logIn,SignUp;


    

    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_Email=findViewById(R.id.email);
        et_password=findViewById(R.id.password);

        logIn=findViewById(R.id.login);
        SignUp=findViewById(R.id.signup);

        sp  = getSharedPreferences("record", Context.MODE_PRIVATE);
        editor = sp.edit();

        String name = sp.getString("name", null);
       // e1.setText(name);

        String phone = sp.getString("phone", null);
       // e2.setText(phone);


        auth=FirebaseAuth.getInstance();

        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              startActivity(new Intent(MainActivity.this,RegisterActivity.class));

            }
        });

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = et_Email.getText().toString();
                String password = et_password.getText().toString();



//                Intent intent=new Intent(MainActivity.this,After_Login.class);
//                intent.putExtra("id",email);
//                startActivity(intent);
                if (email.equals("")) {
                    et_Email.setError("Please Enter Email");
                } else if (password.equals("")) {
                    et_password.setError("Please enter password");
                }
                else {

                    final ProgressDialog dialog=new ProgressDialog(MainActivity.this);
                    dialog.setTitle("please wait");
                    dialog.setCancelable(false);
                    dialog.show();


auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful())
                        {

                           // Toast.makeText(MainActivity.this, ""+task.toString(), Toast.LENGTH_SHORT).show();

                            dialog.dismiss();
                            startActivity(new Intent(MainActivity.this, After_Login.class));
                        } else
                            {

                            dialog.dismiss();
                            //Toast.makeText(MainActivity.this, "Invalid user", Toast.LENGTH_SHORT).show();
                        }

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.dismiss();
 Toast.makeText(MainActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }

            }
        });

    }



    }


