package com.ms.sam.loginfirebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class After_Login extends AppCompatActivity {

    FirebaseAuth auth;
    TextView textView_email;

    private DrawerLayout mDrawerLayout;
    NavigationView view;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
//
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after__login);

        auth=FirebaseAuth.getInstance();
        textView_email=findViewById(R.id.text_id);
//
//        Bundle bundle=getIntent().getExtras();
//
//        textView_.setText(bundle.getString("id"));


        mDrawerLayout = findViewById(R.id.drawer_layout);
        view=findViewById(R.id.nav_view);

        view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem)
            {
                int id=menuItem.getItemId();
                //Toast.makeText(After_Login.this, "check", Toast.LENGTH_SHORT).show();


                if (id == R.id.logout)
                {
                    Toast.makeText(After_Login.this, "logout", Toast.LENGTH_SHORT).show();
                    auth.signOut();
                    startActivity(new Intent(After_Login.this,MainActivity.class));
                }
                return true;
            }
        });

    }



}
